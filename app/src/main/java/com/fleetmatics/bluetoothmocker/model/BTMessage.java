package com.fleetmatics.bluetoothmocker.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class BTMessage {
    public String getTimestamp() {
        return timestamp;
    }

    private final String timestamp;
    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());
    String msg;

    public boolean isDirectionOut() {
        return directionOut;
    }

    boolean directionOut;

    public long getIndex() {
        return index;
    }

    public void setIndex(long newIndex) {
        this.index = newIndex;
    }

    long index;

    public BTMessage(String message, boolean directionOut) {
        this.msg = message;
        this.directionOut = directionOut;
        this.timestamp = df.format(new Date());
        this.index = 0;
    }

    public String getMsg() {
        return msg;
    }
}