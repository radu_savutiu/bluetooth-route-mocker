package com.fleetmatics.bluetoothmocker.model;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class ProtocolItem {
    String msgReceived;
    boolean isRepeating;
    long respondTimes;
    long respondInterval;
    long respondDelay;
    String msgResponse;

    public ProtocolItem() {
    }

    public ProtocolItem(String msgReceived, boolean isRepeating, long respondTimes, long respondInterval, long respondDelay, String msgResponse) {
        this.msgReceived = msgReceived;
        this.isRepeating = isRepeating;
        this.respondTimes = respondTimes;
        this.respondInterval = respondInterval;
        this.respondDelay = respondDelay;
        this.msgResponse = msgResponse;
    }

    public String getMsgReceived() {
        return msgReceived;
    }

    public boolean isRepeating() {
        return isRepeating;
    }

    public long getRespondTimes() {
        return respondTimes;
    }

    public long getRespondInterval() {
        return respondInterval;
    }

    public long getRespondDelay() {
        return respondDelay;
    }

    public String getMsgResponse() {
        return msgResponse;
    }

    @Override
    public String toString() {
        return "ProtocolItem{" +
                "msgReceived='" + msgReceived + '\'' +
                ", isRepeating=" + isRepeating +
                ", respondTimes=" + respondTimes +
                ", respondInterval=" + respondInterval +
                ", respondDelay=" + respondDelay +
                ", msgResponse='" + msgResponse + '\'' +
                '}';
    }
}
