package com.fleetmatics.bluetoothmocker.model;

import android.bluetooth.BluetoothAdapter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.fleetmatics.bluetoothmocker.app.BluetoothMockerApp;
import com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment;
import com.google.android.gms.maps.model.LatLng;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.DERIVED_ENGINE_HOURS_IDLING_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.DERIVED_ENGINE_HOURS_TOTAL_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.DERIVED_ODOMETER_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.ENGINE_HOURS_IDLING_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.ENGINE_HOURS_TOTAL_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.IGNITION_ON_PREF_LEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.IS_DRIVING_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.ODOMETER_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.SPEED_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.SPEED_TEXT_PREF_KEY;
import static com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment.VIN_PREF_KEY;

/**
 * Created by rsavutiu on 27/01/2017.
 */
//Key AIzaSyBDHr316Hy2LMHoan4A6Sz03W_tq1X-BlM
public class VehicleModel {
    private final static SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
    private final SharedPreferences prefs;
    private LatLng location = new LatLng(0, 0);
    private boolean ignition;
    private boolean driving;
    private boolean gpsGood;
    private boolean hasOdometer;
    private boolean hasDerivedOdometer;
    private boolean hasEngineHours;
    private boolean hasDerivedEngineHours;
    private float derivedEngineHoursTotal;
    private float derivedEngineHoursIdling;
    private float engineHoursTotal;
    private boolean speedIsText;
    private float engineHoursIdling;
    private boolean ecmStatus;
    private String VIN;
    private float odometer;
    private float derivedOdometer;
    private float speed;

    public boolean hasOdometer() {
        return hasOdometer;
    }

    public void setHasOdometer(boolean hasOdometer) {
        this.hasOdometer = hasOdometer;
    }

    public boolean hasDerivedOdometer() {
        return hasDerivedOdometer;
    }

    public void setHasDerivedOdometer(boolean hasDerivedOdometer) {
        this.hasDerivedOdometer = hasDerivedOdometer;
    }

    public boolean hasEngineHours() {
        return hasEngineHours;
    }

    public void setHasEngineHours(boolean hasEngineHours) {
        this.hasEngineHours = hasEngineHours;
    }

    public boolean hasDerivedEngineHours() {
        return hasDerivedEngineHours;
    }

    public void setHasDerivedEngineHours(boolean hasDerivedEngineHours) {
        this.hasDerivedEngineHours = hasDerivedEngineHours;
    }

    public void setLocation(LatLng location) {
        this.location = location;
    }

    public void setIgnition(boolean ignition) {
        this.ignition = ignition;
        prefs.edit().putBoolean(IGNITION_ON_PREF_LEY, ignition).apply();
    }

    public void setGPSGood(boolean gpsGood) {
        this.gpsGood = gpsGood;
        prefs.edit().putBoolean(IGNITION_ON_PREF_LEY, ignition).apply();
    }

    public void setDriving(boolean driving) {
        this.driving = driving;
        prefs.edit().putBoolean(IS_DRIVING_PREF_KEY, ignition).apply();
    }

    public boolean isDriving() {
        return driving;
    }

    public boolean isEcmStatus() {
        return ecmStatus;
    }

    public void setEcmStatus(boolean ecmStatus) {
        this.ecmStatus = ecmStatus;
    }

    public void setVIN(String VIN) {
        this.VIN = VIN;
        prefs.edit().putString(VIN_PREF_KEY, VIN).apply();
    }

    public void setOdometer(float newValue) {
        this.odometer = (float) round(newValue, 6);
        prefs.edit().putFloat(ODOMETER_PREF_KEY, odometer).apply();
    }

    public void setDerivedOdometer(float newValue) {
        this.derivedOdometer = (float) round(newValue, 6);
        prefs.edit().putFloat(DERIVED_ODOMETER_PREF_KEY, derivedOdometer).apply();
    }

    private static VehicleModel _instance;

    public VehicleModel() {
        prefs = PreferenceManager.getDefaultSharedPreferences(BluetoothMockerApp.getInstance());
        this.ignition = prefs.getBoolean(VehicleSetupFragment.IGNITION_ON_PREF_LEY, false);
        this.driving = prefs.getBoolean(IS_DRIVING_PREF_KEY, false);
        this.VIN = prefs.getString(VehicleSetupFragment.VIN_PREF_KEY, "");
        this.ignition = prefs.getBoolean(VehicleSetupFragment.IGNITION_ON_PREF_LEY, false);
        this.odometer = prefs.getFloat(VehicleSetupFragment.ODOMETER_PREF_KEY, 0);
        this.derivedOdometer = prefs.getFloat(VehicleSetupFragment.DERIVED_ODOMETER_PREF_KEY, 0);
        this.hasDerivedOdometer = prefs.getBoolean(VehicleSetupFragment.HAS_DERIVED_ODOMETER_PREF_KEY, true);
        this.hasOdometer = prefs.getBoolean(VehicleSetupFragment.HAS_ODOMETER_PREF_KEY, true);
        this.hasEngineHours = prefs.getBoolean(VehicleSetupFragment.HAS_ENGINE_HOURS_PREF_KEY, true);
        this.hasDerivedEngineHours = prefs.getBoolean(VehicleSetupFragment.HAS_DERIVED_ENGINE_HOURS_PREF_KEY, true);
    }

    public static VehicleModel getInstance() {
        if (_instance == null) {
            _instance = new VehicleModel();
        }
        return _instance;
    }

    public String fillIn(String msgResponse) {
        String date = df.format(new Date());

        /*App limits for privacy to 2 decimal points the location. box doesn't do it for us.
        String ret = msgResponse.replace("{lat}", Double.toString(round(location.latitude, 2)));
        ret = ret.replace("{long}", Double.toString(round(location.longitude, 2)));*/
        String ret = msgResponse.replace("{lat}", Double.toString(location.latitude));
        ret = ret.replace("{long}", Double.toString(location.longitude));

        ret = ret.replace("{ignitionStatus}", (ignition) ? "ON" : "OFF");
        ret = ret.replace("{gpsStatus}", (gpsGood) ? "GOOD" : "BAD");
        ret = ret.replace("{ecmStatus}", (ecmStatus) ? "1" : "0");

        ret = ret.replace("{odometer}", Float.toString((long) (hasOdometer ? round(odometer, 3) : 0)));
        ret = ret.replace("{odoDerived}", Float.toString((long) (hasDerivedOdometer ? round(derivedOdometer, 3) : 0)));

        String speedString = VehicleModel.getInstance().getSpeedModeIsText() ? "STOPPED":"0";
        if (driving) {
            if (VehicleModel.getInstance().getSpeedModeIsText()) {
                if (this.speed > 5) {
                    speedString = "MOVING";
                }
                else {
                    speedString = "STOPPED";
                }
            }
            else {
                speedString = Float.toString(this.speed);
            }
        }
        ret = ret.replace("{speed}", speedString);

        ret = ret.replace("{btName}", BluetoothAdapter.getDefaultAdapter().getName());
        ret = ret.replace("{connectionType}", "BusIsOBD");
        ret = ret.replace("{date}", date);

        ret = ret.replace("{vin}", VehicleModel.getInstance().getVin());

        ret = ret.replace("{derivedEngHoursTotal}", hasDerivedEngineHours?
            addDecimalPointZeros(Double.toString(VehicleModel.round(derivedEngineHoursTotal, 3))) : "0.000");
        ret = ret.replace("{engHoursTotal}", hasEngineHours?
            addDecimalPointZeros(Double.toString(VehicleModel.round(engineHoursTotal, 3))) : "0.000");

        ret = ret.replace("{derivedEngHoursIdling}", hasDerivedEngineHours?
            addDecimalPointZeros(Double.toString(VehicleModel.round(derivedEngineHoursIdling, 3))) : "0.000");
        ret = ret.replace("{engHoursIdling}", hasEngineHours ?
            addDecimalPointZeros(Double.toString(VehicleModel.round(engineHoursIdling, 3))) : "0.000");
        return ret;
    }

    private static String addDecimalPointZeros(String num) {
        int pos = num.indexOf(".");
        int initialLen = num.length();
        if (pos==-1) {
            num += ".000";
        }
        else if (pos==initialLen-3) {
            num += "0";
        }
        else if (pos==initialLen-2) {
            num += "00";
        }
        else if (pos<initialLen-3) {
            num = num.substring(0, pos+4);
        }
        pos = num.indexOf(".");
        assert(pos == num.length()-4);
        return num;
    }

    public boolean isIgnitionOn() {
        return ignition;
    }

    public void setSpeed(float speed) {
        prefs.edit().putFloat(SPEED_PREF_KEY, speed).apply();
        this.speed = speed;
    }

    public float getSpeed() {
        return speed;
    }

    public float getOdometer() {
        return odometer;
    }

    public float getDerivedOdometer() {
        return derivedOdometer;
    }

    public boolean isGpsGood() {
        return gpsGood;
    }

    public String getVin() {
        return VIN;
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static float round(float value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    public void setDerivedEngineHoursTotal(float derivedEngineHoursTotal) {
        this.derivedEngineHoursTotal = derivedEngineHoursTotal;
        prefs.edit().putFloat(DERIVED_ENGINE_HOURS_TOTAL_PREF_KEY, derivedEngineHoursTotal).apply();
    }

    public void setDerivedEngineHoursIdling(float derivedEngineHoursIdling) {
        this.derivedEngineHoursIdling = derivedEngineHoursIdling;
        prefs.edit().putFloat(DERIVED_ENGINE_HOURS_IDLING_PREF_KEY, derivedEngineHoursIdling).apply();
    }

    public void setEngineHoursTotal(float engineHoursTotal) {
        this.engineHoursTotal = engineHoursTotal;
        prefs.edit().putFloat(ENGINE_HOURS_TOTAL_PREF_KEY, engineHoursTotal).apply();
    }

    public void setEngineHoursIdling(float engineHoursIdling) {
        this.engineHoursIdling = engineHoursIdling;
        prefs.edit().putFloat(ENGINE_HOURS_IDLING_PREF_KEY, engineHoursIdling).apply();
    }

    public float getDerivedEngineHoursTotal() {
        return VehicleModel.round(derivedEngineHoursTotal, 3);
    }

    public float getDerivedEngineHoursIdling() {
        return VehicleModel.round(derivedEngineHoursIdling, 3);
    }

    public float getEngineHoursTotal() {
        return VehicleModel.round(engineHoursTotal, 3);
    }

    public float getEngineHoursIdling() {
        return VehicleModel.round(engineHoursIdling, 3);
    }

    public void setSpeedIsText(boolean speedIsText) {
        this.speedIsText = speedIsText;
        prefs.edit().putBoolean(SPEED_TEXT_PREF_KEY, speedIsText).apply();
    }

    public boolean getSpeedModeIsText() {
        return this.speedIsText;
    }
}
