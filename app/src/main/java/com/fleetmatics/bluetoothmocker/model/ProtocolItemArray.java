package com.fleetmatics.bluetoothmocker.model;

import android.content.res.Resources;
import android.text.TextUtils;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class ProtocolItemArray {
    private static ProtocolItemArray protocolMessages = null;

    public static void setSelectedBTFriendlyName(String name) {
        selectedBTFriendlyName = name;
    }

    public ProtocolItem[] getMessages() {
        return messages;
    }

    ProtocolItem[] messages;

    public String[] getBTFriendlyNames() {
        return btFriendlyNames;
    }

    String[] btFriendlyNames;

    private static transient String selectedBTFriendlyName;

    public static ProtocolItemArray getFromFile(Resources resources) {
        if (protocolMessages == null) {
            try {
                InputStream is = resources.getAssets().open("protocol.json");
                byte[] buffer = new byte[is.available()];
                is.read(buffer);
                is.close();
                String responseString = new String(buffer);
                Gson gson = new Gson();
                protocolMessages = gson.fromJson(responseString, ProtocolItemArray.class);
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException("Could not open protocol.json");
            }
        }
        return protocolMessages;
    }

    public static String getSelecteBTFriendlyName(Resources resources) {
        if (TextUtils.isEmpty(selectedBTFriendlyName)) {
            if (protocolMessages == null) {
                protocolMessages = getFromFile(resources);
            }
            if (protocolMessages != null && protocolMessages.btFriendlyNames != null &&
                    protocolMessages.btFriendlyNames.length > 0) {
                selectedBTFriendlyName = protocolMessages.btFriendlyNames[0];
            }
        }
        return selectedBTFriendlyName;
    }
}
