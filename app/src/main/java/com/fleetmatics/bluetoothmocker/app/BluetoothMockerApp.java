package com.fleetmatics.bluetoothmocker.app;

import android.app.Application;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDexApplication;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class BluetoothMockerApp extends MultiDexApplication {
    private static BluetoothMockerApp _instance;

    public static BluetoothMockerApp getInstance() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        _instance = this;
    }
}
