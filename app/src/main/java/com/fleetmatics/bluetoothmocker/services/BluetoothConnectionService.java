package com.fleetmatics.bluetoothmocker.services;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.text.TextUtils;

import com.fleetmatics.bluetoothmocker.model.BTMessage;
import com.fleetmatics.bluetoothmocker.model.ProtocolItem;
import com.fleetmatics.bluetoothmocker.model.ProtocolItemArray;
import com.fleetmatics.bluetoothmocker.model.VehicleModel;
import com.fleetmatics.bluetoothmocker.utils.Logger;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by rsavutiu on 26/01/2017.
 */
public class BluetoothConnectionService extends IntentService {
    private static final String TAG = "BluetoothConnectionService";
    public static AtomicReference<String> sendMessage = new AtomicReference<>();
    public BluetoothConnectionService() {
        super("BluetoothConnectionService");
    }

    public BluetoothConnectionService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        ProtocolItemArray protocolMessages = ProtocolItemArray.getFromFile(getResources());

        byte[] tempBuffer = new byte[1024];
        BluetoothSocket socket = BluetoothConnectionManager.getInstance().getConnectedSocket();
        while (socket!=null && socket.isConnected()) {
            try {
                int readBytes = socket.getInputStream().read(tempBuffer);
                String message = new String(tempBuffer, 0, readBytes);
                Logger.getLogger().i(TAG, "Read msg: " + message);
                BTMessage rcvdMessage = new BTMessage(message, false);
                EventBus.getDefault().post(rcvdMessage);

                if (protocolMessages!=null && protocolMessages.getMessages()!=null) {
                    for (ProtocolItem protocolItem : protocolMessages.getMessages()) {
                        if (protocolItem.getMsgReceived()!=null &&
                            protocolItem.getMsgReceived().equals(rcvdMessage.getMsg())) {
                            sendMessage.set(VehicleModel.getInstance().fillIn(protocolItem.getMsgResponse()));
                            break;
                        }
                    }
                }

                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (!TextUtils.isEmpty(sendMessage.get())) {
                    final String sendMsg = sendMessage.get();
                    sendMessage.set("");
                    socket.getOutputStream().write(sendMsg.getBytes());
                    Logger.getLogger().i(TAG, "Read msg: " + sendMsg);
                    BTMessage sntMessage = new BTMessage(sendMsg, true);
                    EventBus.getDefault().post(sntMessage);
                }
            } catch (IOException e) {
                Logger.getLogger().e(TAG, "socket operation failed! ", e);
                e.printStackTrace();
                break;
            }
        }
        BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.DISCONNECTED);
    }

}
