package com.fleetmatics.bluetoothmocker.services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import com.fleetmatics.bluetoothmocker.app.BluetoothMockerApp;
import com.fleetmatics.bluetoothmocker.ui.TabbedActivity;
import com.fleetmatics.bluetoothmocker.utils.Logger;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.fleetmatics.bluetoothmocker.services.BluetoothServerService.START_CONNECTION;
import static com.fleetmatics.bluetoothmocker.services.BluetoothServerService.STOP_CONNECTION;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class BluetoothConnectionManager{
    private static final String TAG = "BluetoothConnectionManager";
    private static BluetoothConnectionManager _instance;
    private BLUETOOTH_STATE bluetoothState = BLUETOOTH_STATE.ADAPTER_DISABLED;
    private List<BluetoothDevice> foundDevices;


    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                foundDevices.add(device);
            }
        }
    };
    private BluetoothSocket bluetoothSocket;
    private BluetoothSocket connectedSocket;

    public void setConnectedSocket(BluetoothSocket connectedSocket) {
        this.connectedSocket = connectedSocket;
    }

    public BluetoothSocket getConnectedSocket() {
        return connectedSocket;
    }

    public BLUETOOTH_STATE getBluetoothState() {
        return bluetoothState;
    }

    public void setBluetoothState(BLUETOOTH_STATE bluetoothState) {
        this.bluetoothState = bluetoothState;
        EventBus.getDefault().post(bluetoothState);
    }

    public enum BLUETOOTH_STATE {ADAPTER_DISABLED, ADAPTER_ENABLED, ENABLING_DISCOVERABLE, DISCOVERING, CONNECTING_SERVER, CONNECTING_CLIENT, CONNECTED, DISCONNECTED};

    public static BluetoothConnectionManager getInstance() {
        if (_instance==null) {
            _instance = new BluetoothConnectionManager();
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                _instance.setBluetoothState(BLUETOOTH_STATE.ADAPTER_ENABLED);
            }
        }
        return _instance;
    }

    public void startDiscovery() {
        Logger.getLogger().i(TAG, "startDiscovery");
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
        }
        foundDevices = new ArrayList<>();
        BluetoothAdapter.getDefaultAdapter().startDiscovery();
        bluetoothState = BLUETOOTH_STATE.DISCOVERING;
        // Register for broadcasts when a device is discovered.
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        BluetoothMockerApp.getInstance().registerReceiver(mReceiver, filter);

    }

    public void stopDiscovery() {
        Logger.getLogger().i(TAG, "stopDiscovery");
        if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
        }
        bluetoothState = BLUETOOTH_STATE.DISCONNECTED;
        BluetoothMockerApp.getInstance().unregisterReceiver(mReceiver);
    }

    public void startServer(FragmentActivity appCompatActivity) {
        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            BluetoothAdapter.getDefaultAdapter().enable();
        }
        bluetoothState = BLUETOOTH_STATE.ENABLING_DISCOVERABLE;
        if (BluetoothAdapter.getDefaultAdapter().getScanMode()!=BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Logger.getLogger().i(TAG, "startServer enable discoverable");
            Intent discoverableIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 600);
            appCompatActivity.startActivityForResult(discoverableIntent, TabbedActivity.REQUEST_DISCOVERABLE_CODE);
        }
        else {
            Logger.getLogger().i(TAG, "startServer go through");
            bluetoothState = BLUETOOTH_STATE.CONNECTING_SERVER;
            Intent serviceIntent = new Intent(BluetoothMockerApp.getInstance(), BluetoothServerService.class);
            serviceIntent.setAction(START_CONNECTION);
            BluetoothMockerApp.getInstance().startService(serviceIntent);
        }
    }

    public void stopServer(FragmentActivity activity) {
        Logger.getLogger().i(TAG, "stopServer");
        if (BluetoothAdapter.getDefaultAdapter().getScanMode()==BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent serviceIntent = new Intent(activity, BluetoothServerService.class);
            serviceIntent.setAction(STOP_CONNECTION);
            BluetoothMockerApp.getInstance().startService(serviceIntent);
        }
    }
}
