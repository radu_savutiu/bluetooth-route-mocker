package com.fleetmatics.bluetoothmocker.services;

import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;

import com.fleetmatics.bluetoothmocker.model.ProtocolItemArray;
import com.fleetmatics.bluetoothmocker.ui.TabbedActivity;
import com.fleetmatics.bluetoothmocker.utils.Logger;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class BluetoothServerService extends IntentService {
    private final static String TAG = "BluetoothServerService";
    public static final String START_CONNECTION = "START_CONNECTION";
    public static final String STOP_CONNECTION = "STOP_CONNECTION";
    //SPP UUID
    private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothServerSocket serverSocket;

    public BluetoothServerService() {
        super("BluetoothServerService");
    }

    public BluetoothServerService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent.getAction().equals(START_CONNECTION)) {
            startServerConnection();
        } else if (intent.getAction().equals(STOP_CONNECTION)) {
            stopServerConnection();
        }
    }

    private void stopServerConnection() {
        if (serverSocket != null) {
            try {
                serverSocket.close();
            } catch (IOException e) {
                Logger.getLogger().e(TAG, "Stop server connection failed", e);
                e.printStackTrace();
            }
        }
        if (BluetoothConnectionManager.getInstance().getBluetoothState() != BluetoothConnectionManager.BLUETOOTH_STATE.CONNECTED) {
            BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.DISCONNECTED);
        }

        cancelBluetoothDiscoverable();
    }

    private void cancelBluetoothDiscoverable() {
        if (BluetoothAdapter.getDefaultAdapter().getScanMode() == BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent =
                    new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 1);
            discoverableIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getBaseContext().startActivity(discoverableIntent);
        }
    }

    private void startServerConnection() {
        Logger.getLogger().i(TAG, "Start server connection");
        if (serverSocket != null) {
            stopServerConnection();
        }
        BluetoothAdapter.getDefaultAdapter().setName(ProtocolItemArray.getSelecteBTFriendlyName(getResources()));
        try {
            serverSocket = BluetoothAdapter.getDefaultAdapter().listenUsingInsecureRfcommWithServiceRecord("GNEX_Mocker", SPP_UUID);
            BluetoothSocket bluetoothSocket = serverSocket.accept();
            if (bluetoothSocket != null) {
                BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.CONNECTED);
                Intent connectionServiceIntent = new Intent(getBaseContext(), BluetoothConnectionService.class);
                BluetoothConnectionManager.getInstance().setConnectedSocket(bluetoothSocket);
                startService(connectionServiceIntent);
            }
        } catch (IOException e) {
            Logger.getLogger().e(TAG, "Could not open server socket or accept connection", e);
            e.printStackTrace();
            BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.DISCONNECTED);
        } finally {
            if (serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    Logger.getLogger().e(TAG, "Could not close server socket", e);
                    e.printStackTrace();
                }
            }
            if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                cancelBluetoothDiscoverable();
            }
        }
    }
}
