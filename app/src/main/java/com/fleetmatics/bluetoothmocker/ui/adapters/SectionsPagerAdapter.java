package com.fleetmatics.bluetoothmocker.ui.adapters;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.fleetmatics.bluetoothmocker.ui.fragments.BluetoothConnectedFragment;
import com.fleetmatics.bluetoothmocker.ui.fragments.BluetoothConnectionFragment;
import com.fleetmatics.bluetoothmocker.ui.fragments.MapContainerFragment;
import com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment;

public class SectionsPagerAdapter extends FragmentStatePagerAdapter {
    private final FragmentManager fm;
    private MapContainerFragment mapFragment;
    private VehicleSetupFragment vehicleFragment;
    private BluetoothConnectionFragment btConnectionFragment;

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
        this.fm = fm;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        if (position == 0) {
            btConnectionFragment = BluetoothConnectionFragment.newInstance();
            return btConnectionFragment;
        }
        if (position == 1) {
            return BluetoothConnectedFragment.newInstance();
        }
        else if (position==2) {
            vehicleFragment = VehicleSetupFragment.newInstance();
            return vehicleFragment;
        }
        if (position == 3) {
            mapFragment = MapContainerFragment.newInstance();
            return mapFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        // Show 4 total pages.
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Setup";
            case 1:
                return "Messages";
            case 2:
                return "Vehicle";
            case 3:
                return "Map";
        }
        return null;
    }

    @Nullable
    public MapContainerFragment getMapFragment() {
        return mapFragment;
    }

    @Nullable
    public VehicleSetupFragment getVehicleFragment() {
        return vehicleFragment;
    }

    @Nullable
    public BluetoothConnectionFragment getBTConnectionFragment() {
        return btConnectionFragment;
    }
}