package com.fleetmatics.bluetoothmocker.ui;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.fleetmatics.bluetoothmocker.R;
import com.fleetmatics.bluetoothmocker.model.VehicleModel;
import com.fleetmatics.bluetoothmocker.services.BluetoothConnectionManager;
import com.fleetmatics.bluetoothmocker.ui.adapters.SectionsPagerAdapter;
import com.fleetmatics.bluetoothmocker.ui.fragments.BluetoothConnectionFragment;
import com.fleetmatics.bluetoothmocker.ui.fragments.MapContainerFragment;
import com.fleetmatics.bluetoothmocker.ui.fragments.VehicleSetupFragment;
import com.fleetmatics.bluetoothmocker.utils.Logger;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TabbedActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final static String TAG = "Main";
    public static final int REQUEST_DISCOVERABLE_CODE = 143;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    @BindView(R.id.container)
    protected ViewPager mViewPager;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.tabs)
    protected TabLayout tabLayout;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private Location lastLocation;

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive (Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
                BluetoothConnectionFragment btConnectionFragment = mSectionsPagerAdapter.getBTConnectionFragment();
                int extraState = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1);
                if (btConnectionFragment!=null) {
                    if (extraState == BluetoothAdapter.STATE_ON) {
                        BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.ADAPTER_ENABLED);
                        btConnectionFragment.changeUIOnBluetoothState();
                    } else if (extraState == BluetoothAdapter.STATE_OFF) {
                        BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.ADAPTER_DISABLED);
                        btConnectionFragment.changeUIOnBluetoothState();
                    }
                }
            }

        }

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.getLogger().i(TAG, "onActivityResult");
        if (requestCode == REQUEST_DISCOVERABLE_CODE) {
            if (resultCode == RESULT_CANCELED) {
                Logger.getLogger().i(TAG, "Discoverable ended");
                BluetoothConnectionManager.getInstance().setBluetoothState(BluetoothConnectionManager.BLUETOOTH_STATE.DISCONNECTED);
            } else {
                Logger.getLogger().i(TAG, "Discoverable started");
                BluetoothConnectionManager.getInstance().startServer(this);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.getLogger().i(TAG, "onCreate");
        setContentView(R.layout.activity_tabbed);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(5);

        tabLayout.setupWithViewPager(mViewPager);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();

        // Create the LocationRequest object
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                .setFastestInterval(1 * 1000); // 1 second, in milliseconds

        registerReceiver(mReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Logger.getLogger().i(TAG, "onCreateOptionsMenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Logger.getLogger().i(TAG, "onOptionsItemSelected: " + id);
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        Logger.getLogger().i(TAG, "onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.getLogger().i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Logger.getLogger().i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        Logger.getLogger().i(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.getLogger().i(TAG, "onDestroy");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        mGoogleApiClient.disconnect();
        if (mReceiver!=null) {
            unregisterReceiver(mReceiver);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBluetoothStateChanged(BluetoothConnectionManager.BLUETOOTH_STATE newState) {
        if (newState == BluetoothConnectionManager.BLUETOOTH_STATE.CONNECTED) {
            mViewPager.setCurrentItem(1);
        } else {
            mViewPager.setCurrentItem(0);
        }
    }

    protected void createLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        if (location!=null) {
            handleLocation(location);
        }
    }

    private void handleLocation(@NonNull Location location) {
        MapContainerFragment mapFragment = mSectionsPagerAdapter.getMapFragment();
        Logger.getLogger().i(TAG, "Handle location: " + location);
        final VehicleModel vehicle = VehicleModel.getInstance();
        if (vehicle.isDriving() && vehicle.isIgnitionOn()) {
            vehicle.setLocation(new LatLng(location.getLatitude(), location.getLongitude()));

            //m/s to km/h.
            vehicle.setSpeed(location.getSpeed() * 3.6f);

            if (lastLocation!=null) {
                float newDistance = ((location.distanceTo(lastLocation)) / 1000f);
                if (vehicle.hasDerivedOdometer()) {
                    vehicle.setDerivedOdometer(vehicle.getDerivedOdometer() + newDistance);
                }

                if (vehicle.hasOdometer()) {
                    vehicle.setOdometer(vehicle.getOdometer() + newDistance);
                }

                if (mapFragment!=null) {
                    mapFragment.addMarker(lastLocation);
                }

                String engHoursTotal = "0.000", engHoursIdling = "0.000", derivedEngHours = "0.000", derivedEngHoursIdling = "0.000";
                if (vehicle.isIgnitionOn() && (vehicle.hasDerivedEngineHours() || vehicle.hasEngineHours())) {
                    long deltaInSeconds = (location.getTime() - lastLocation.getTime()) / 1000;
                    float deltaInHours = deltaInSeconds / 3600f;
                    if (!vehicle.isDriving() || !(vehicle.getSpeed()>5)) {
                        if (vehicle.hasDerivedEngineHours()) {
                            vehicle.setDerivedEngineHoursIdling(vehicle.getDerivedEngineHoursIdling() + deltaInHours);
                        }
                        if (vehicle.hasEngineHours()) {
                            vehicle.setEngineHoursIdling(vehicle.getEngineHoursIdling() + deltaInHours);
                        }
                    }

                    if (vehicle.hasDerivedEngineHours()) {
                        vehicle.setDerivedEngineHoursTotal(vehicle.getDerivedEngineHoursTotal() + deltaInHours);
                    }

                    if (vehicle.hasEngineHours()) {
                        vehicle.setEngineHoursTotal(vehicle.getEngineHoursTotal() + deltaInHours);
                    }
                }

            }
        }

        VehicleSetupFragment vehicleSetupFragment = mSectionsPagerAdapter.getVehicleFragment();
        vehicleSetupFragment.updateUI(false);

        lastLocation = location;
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location!=null) {
            handleLocation(location);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

}
