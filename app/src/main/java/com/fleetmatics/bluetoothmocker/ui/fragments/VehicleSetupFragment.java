package com.fleetmatics.bluetoothmocker.ui.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.fleetmatics.bluetoothmocker.R;
import com.fleetmatics.bluetoothmocker.model.VehicleModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * A placeholder fragment containing a simple view.
 */
public class VehicleSetupFragment extends Fragment {
    public static final String IS_DRIVING_PREF_KEY = "IS_DRIVING_PREF_KEY";
    public static final String HAS_ODOMETER_PREF_KEY = "HAS_ODOMETER_PREF_KEY";

    public static final String HAS_ENGINE_HOURS_PREF_KEY = "HAS_ENGINE_HOURS_PREF_KEY";
    public static final String HAS_DERIVED_ENGINE_HOURS_PREF_KEY = "HAS_DERIVED_ENGINE_HOURS_PREF_KEY";

    public static final String HAS_DERIVED_ODOMETER_PREF_KEY = "HAS_DERIVED_ODOMETER_PREF_KEY";
    public static final String IGNITION_ON_PREF_LEY = "IGNITION_ON_PREF_LEY";
    public static final String GPS_GOOD_KEY = "GPS_GOOD_KEY";
    public static final String VIN_PREF_KEY = "VIN_PREF_KEY";
    public static final String ODOMETER_PREF_KEY = "ODOMETER_PREF_KEY";
    public static final String DERIVED_ODOMETER_PREF_KEY = "DERIVED_ODOMETER_PREF_KEY";
    public static final String SPEED_PREF_KEY = "SPEED_PREF_KEY";

    public static final String ENGINE_HOURS_TOTAL_PREF_KEY = "ENGINE_HOURS_TOTAL_PREF_KEY";
    public static final String DERIVED_ENGINE_HOURS_TOTAL_PREF_KEY = "DERIVED_ENGINE_HOURS_TOTAL_PREF_KEY";

    public static final String ENGINE_HOURS_IDLING_PREF_KEY = "ENGINE_HOURS_IDLING_PREF_KEY";
    public static final String DERIVED_ENGINE_HOURS_IDLING_PREF_KEY = "DERIVED_ENGINE_HOURS_IDLING_PREF_KEY";

    public static final String SPEED_TEXT_PREF_KEY = "SPEED_TEXT_PREF_KEY";

    @BindView(R.id.tvOdometer)
    TextView tvOdometer;

    @BindView(R.id.tvDerivedOdometer)
    TextView tvDerivedOdometer;

    @BindView(R.id.tvEngineHours)
    TextView tvEngineHours;

    @BindView(R.id.tvDerivedEngineHours)
    TextView tvDerivedEngineHours;

    @BindView(R.id.swDriving)
    Switch swDriving;

    @BindView(R.id.swIgnition)
    Switch swIgnition;

    @BindView(R.id.swGPS)
    Switch swGPS;

    @BindView(R.id.swEcm)
    Switch swEcm;

    @BindView(R.id.swSpeedType)
    Switch swSpeedType;

    @BindView(R.id.etVIN)
    EditText etVIN;

    @BindView(R.id.swEngineHours)
    Switch swEngineHours;

    @BindView(R.id.swDerivedEngineHours)
    Switch swDerivedEngineHours;

    @BindView(R.id.swDerivedOdometer)
    Switch swDerivedOdometer;

    @BindView(R.id.swOdometer)
    Switch swOdometer;

    private SharedPreferences prefs;
    public VehicleSetupFragment() {
    }

    public static VehicleSetupFragment newInstance() {
        VehicleSetupFragment fragment = new VehicleSetupFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_vehicle_setup, container, false);
        prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swDriving.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                VehicleModel.getInstance().setDriving(b);
            }
        });

        swIgnition.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                VehicleModel.getInstance().setIgnition(b);
            }
        });

        swGPS.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                VehicleModel.getInstance().setGPSGood(b);
            }
        });

        swDerivedOdometer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setHasDerivedOdometer(isChecked);
            }
        });

        swOdometer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setHasOdometer(isChecked);
            }
        });

        swEcm.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setEcmStatus(isChecked);
            }
        });

        swSpeedType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setSpeedIsText(isChecked);
            }
        });

        swDerivedEngineHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setHasDerivedEngineHours(isChecked);
            }
        });

        swEngineHours.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                VehicleModel.getInstance().setHasEngineHours(isChecked);
            }
        });

        etVIN.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                VehicleModel.getInstance().setVIN(etVIN.getText().toString());
            }
        });

        updateUI(true);
    }

    public void updateUI(boolean updateVIN) {
        VehicleModel vehicle = VehicleModel.getInstance();
        swDriving.setChecked(vehicle.isDriving());
        swIgnition.setChecked(vehicle.isIgnitionOn());
        tvOdometer.setText(Float.toString(vehicle.getOdometer()));
        tvDerivedOdometer.setText(Float.toString(vehicle.getDerivedOdometer()));
        tvEngineHours.setText(Float.toString(vehicle.getEngineHoursTotal())  + "/" + Float.toString(vehicle.getEngineHoursIdling()));
        tvDerivedEngineHours.setText(Float.toString(vehicle.getDerivedEngineHoursTotal()) + "/" + Float.toString(vehicle.getDerivedEngineHoursIdling()));
        swGPS.setChecked(vehicle.isGpsGood());
        swDerivedOdometer.setChecked(vehicle.hasDerivedOdometer());
        swOdometer.setChecked(vehicle.hasOdometer());
        swDerivedEngineHours.setChecked(vehicle.hasDerivedEngineHours());
        swEngineHours.setChecked(vehicle.hasEngineHours());
        swSpeedType.setChecked(vehicle.getSpeedModeIsText());
        if (updateVIN) {
            etVIN.setText(vehicle.getVin());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateUI(false);
    }

    @OnClick(R.id.rstDerivedOdometerButton)
    public void onResetDerivedOdometer(View view) {
        VehicleModel.getInstance().setDerivedOdometer(0);
    }

    @OnClick(R.id.rstOdometerButton)
    public void onResetOdometer(View view) {
        VehicleModel.getInstance().setOdometer(0);
    }

    @OnClick(R.id.rstDerivedEngineHoursButton)
    public void onResetDerivedEngineHours(View view) {
        VehicleModel.getInstance().setDerivedEngineHoursTotal(0);
        VehicleModel.getInstance().setDerivedEngineHoursIdling(0);
    }

    @OnClick(R.id.rstEngineHoursButton)
    public void onResetEngineHours(View view) {
        VehicleModel.getInstance().setEngineHoursTotal(0);
        VehicleModel.getInstance().setEngineHoursIdling(0);
    }
}