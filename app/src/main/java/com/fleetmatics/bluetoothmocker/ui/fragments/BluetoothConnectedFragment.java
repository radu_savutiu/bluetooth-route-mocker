package com.fleetmatics.bluetoothmocker.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ToggleButton;

import com.fleetmatics.bluetoothmocker.R;
import com.fleetmatics.bluetoothmocker.model.BTMessage;
import com.fleetmatics.bluetoothmocker.services.BluetoothConnectionManager;
import com.fleetmatics.bluetoothmocker.ui.adapters.MessageAdapter;
import com.fleetmatics.bluetoothmocker.utils.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class BluetoothConnectedFragment extends Fragment {
    private static final String TAG = "BluetoothConnectedFragment";
    @BindView(R.id.rvConnection)
    RecyclerView rvConnection;
    @BindView(R.id.tbAutoscroll)
    ToggleButton toggleButtonAutoscroll;
    private MessageAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_bluetooth_connection, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        Logger.getLogger().i(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.getLogger().i(TAG, "onStart");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.getLogger().i(TAG, "onStop");
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBluetoothConnectionChange(BluetoothConnectionManager.BLUETOOTH_STATE newState) {
        changeUIOnBluetoothState();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageAdded(BTMessage msg) {
        msg.setIndex(mAdapter.getMessages().size());
        mAdapter.getMessages().add(0, msg);
        mAdapter.notifyItemInserted(0);
        if (toggleButtonAutoscroll.isChecked()) {
            rvConnection.scrollToPosition(0);
        }
    }

    private void changeUIOnBluetoothState() {
        switch (BluetoothConnectionManager.getInstance().getBluetoothState()) {
            case CONNECTED:
                rvConnection.setEnabled(true);
                break;

            default:
                rvConnection.setEnabled(false);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        changeUIOnBluetoothState();
        rvConnection.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new MessageAdapter(getContext());
        rvConnection.setAdapter(mAdapter);


    }

    public static BluetoothConnectedFragment newInstance() {
        BluetoothConnectedFragment fragment = new BluetoothConnectedFragment();
        return fragment;
    }


}
