package com.fleetmatics.bluetoothmocker.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fleetmatics.bluetoothmocker.R;
import com.fleetmatics.bluetoothmocker.model.BTMessage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsavutiu on 26/01/2017.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    public List<BTMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<BTMessage> messages) {
        this.messages = messages;
    }

    List<BTMessage> messages = new ArrayList<>();
    Context mContext;

    public MessageAdapter(Context context) {
        mContext = context;
    }

    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.message_row, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        final BTMessage btMessage = messages.get(position);
        holder.tvMessage.setText(btMessage.getMsg());
        if (btMessage.isDirectionOut()) {
            holder.tvMessage.setTextColor(mContext.getResources().getColor(R.color.colorAccent));
        }
        else {
            holder.tvMessage.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        }
        holder.tvIndex.setText(Integer.toString(holder.getAdapterPosition() ));
        holder.tvTimestamp.setText(btMessage.getTimestamp());
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvMessage)
        TextView tvMessage;

        @BindView(R.id.tvIndex)
        TextView tvIndex;

        @BindView(R.id.tvTimestamp)
        TextView tvTimestamp;

        public MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
