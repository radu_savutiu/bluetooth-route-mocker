package com.fleetmatics.bluetoothmocker.ui.fragments;

import android.bluetooth.BluetoothAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.fleetmatics.bluetoothmocker.R;
import com.fleetmatics.bluetoothmocker.model.ProtocolItemArray;
import com.fleetmatics.bluetoothmocker.services.BluetoothConnectionManager;
import com.fleetmatics.bluetoothmocker.utils.Logger;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rsavutiu on 26/01/2017.
 */

public class BluetoothConnectionFragment extends Fragment {
    private static final String TAG = "BluetoothConnectionFragment";

    @BindView(R.id.tvState)
    TextView tvState;

    @BindView(R.id.swDiscovery)
    Switch swDiscovery;

    @BindView(R.id.swServer)
    Switch swServer;

    @BindView(R.id.swBtAdapter)
    Switch swBTAdapter;

    @BindView(R.id.spinnerBtName)
    Spinner spinner;

    @BindView(R.id.rvDevices)
    RecyclerView rvBluetoothDevices;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_bluetooth_setup, null);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        Logger.getLogger().i(TAG, "onResume");
        super.onResume();
        changeUIOnBluetoothState();
    }

    @Override
    public void onStart() {
        super.onStart();
        Logger.getLogger().i(TAG, "onStart");
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        Logger.getLogger().i(TAG, "onStop");
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onBluetoothConnectionChange(BluetoothConnectionManager.BLUETOOTH_STATE newState) {
        changeUIOnBluetoothState();
    }

    public void             changeUIOnBluetoothState() {
        swBTAdapter.setChecked(BluetoothAdapter.getDefaultAdapter().isEnabled());
        //swDiscovery.setEnabled(BluetoothAdapter.getDefaultAdapter().isEnabled());
        swServer.setEnabled(BluetoothAdapter.getDefaultAdapter().isEnabled());
        switch (BluetoothConnectionManager.getInstance().getBluetoothState()) {
            case CONNECTED:
                rvBluetoothDevices.setEnabled(false);
                swDiscovery.setEnabled(false);
                swServer.setEnabled(false);
                tvState.setText(getString(R.string.connected));
                break;

            case DISCONNECTED:
                rvBluetoothDevices.setEnabled(true);
                //TODO: Client mode not yet done!
                //swDiscovery.setEnabled(true);
                swDiscovery.setEnabled(false);

                swServer.setEnabled(true);
                swDiscovery.setChecked(false);
                swServer.setChecked(false);
                tvState.setText(getString(R.string.disconnected));
                break;

            case ENABLING_DISCOVERABLE:
                swDiscovery.setEnabled(true);
                swDiscovery.setChecked(true);
                tvState.setText(getString(R.string.enabling_discoverable));
                break;

            case DISCOVERING:
                swDiscovery.setEnabled(true);
                swDiscovery.setChecked(true);
                tvState.setText(getString(R.string.discovering));
                break;

            case CONNECTING_SERVER:
                swServer.setEnabled(true);
                swServer.setChecked(true);
                tvState.setText(getString(R.string.connecting_server));
                break;

            case ADAPTER_DISABLED:
                swServer.setEnabled(false);
                swServer.setChecked(false);
                swDiscovery.setEnabled(false);
                swDiscovery.setChecked(false);
                tvState.setText(getString(R.string.adapter_disabled));
                break;

            default:
                if (BluetoothAdapter.getDefaultAdapter().isEnabled()) {
                    tvState.setText(getString(R.string.adapter_enabled));
                }
                else {
                    tvState.setText(getString(R.string.adapter_disabled));
                }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Logger.getLogger().i(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
        changeUIOnBluetoothState();

        swDiscovery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (swDiscovery.isChecked()) {
                    BluetoothConnectionManager.getInstance().startDiscovery();
                }
                else {
                    BluetoothConnectionManager.getInstance().stopDiscovery();
                }
            }
        });

        swServer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (swServer.isChecked()) {
                    BluetoothConnectionManager.getInstance().startServer(getActivity());
                }
                else {
                    BluetoothConnectionManager.getInstance().stopServer(getActivity());
                }
            }
        });

        swBTAdapter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    BluetoothAdapter.getDefaultAdapter().enable();
                }
                else {
                    BluetoothAdapter.getDefaultAdapter().disable();
                }
            }
        });

        ProtocolItemArray protocolItemArray = ProtocolItemArray.getFromFile(getResources());
        final List<String> friendlyNames = Arrays.asList(protocolItemArray.getBTFriendlyNames());

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(
                getContext(), android.R.layout.simple_spinner_item, friendlyNames);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ProtocolItemArray.setSelectedBTFriendlyName(friendlyNames.get(i));
                BluetoothAdapter.getDefaultAdapter().setName(friendlyNames.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public static BluetoothConnectionFragment newInstance() {
        BluetoothConnectionFragment fragment = new BluetoothConnectionFragment();
        return fragment;
    }
}
